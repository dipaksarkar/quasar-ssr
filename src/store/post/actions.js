import axios from 'axios'

export function getPosts ({ commit }, playload) {
  return new Promise((resolve, reject) => {
    axios.get('https://jsonplaceholder.typicode.com/posts').then(response => {
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}
export function getPost ({ commit }, playload) {
  return new Promise((resolve, reject) => {
    axios.get(`https://jsonplaceholder.typicode.com/posts/${playload}`).then(response => {
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}
