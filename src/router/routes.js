
const routes = [
  {
    path: '/',
    component: () => import('layouts/Main.vue'),
    children: [
      { path: '', name: 'Home', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/posts',
    component: () => import('layouts/Main.vue'),
    children: [
      { path: '', name: 'Posts', component: () => import('pages/posts/index.vue') },
      { path: ':id', name: 'Post', component: () => import('pages/posts/post.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
